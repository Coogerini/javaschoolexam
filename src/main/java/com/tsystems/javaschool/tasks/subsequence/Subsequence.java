package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        //check arguments for correctness
        if ((x == null) || (y == null)) throw new IllegalArgumentException();
        //if ix is empty, then instant return true
        if (x.isEmpty()) {
            return true;
        }
        //at this point x not empty, so if y empty, then return false
        if (y.isEmpty()) {
            return false;
        }
        int yIndex = 0;
        int xIndex = 0;
        boolean check = false;
        while (xIndex < x.size()) {
            check = false;
            Object xValue = x.get(xIndex);
            while (yIndex < y.size() && !check) {
                if (y.get(yIndex).equals(xValue)) {
                    check = true;
                }
                yIndex++;
            }
            xIndex++;
        }


        return check;
    }
}
