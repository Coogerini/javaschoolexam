package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (!validateStatement(statement)) return null;
        try {
            Double result = calculateStatement(statement);
            if (result == Math.floor(result)) {
                return Integer.toString(result.intValue());
            }
            return Double.toString(result);
        } catch (UnsupportedOperationException e) {
            return null;
        }
    }

    private boolean validateStatement(String s) {
        if (s == null || s.isEmpty()) return false;
        char[] ch = s.toCharArray();
        boolean checkOp = true, checkDot = true;
        int countSkob = 0;
        for (int i = 0; i < s.length(); i++) {
            if (!isOper(ch[i]) && !isOperand(ch[i]) && ch[i] != ')' && ch[i] != '(') return false;
            if (ch[i] == ')') countSkob++;
            if (ch[i] == '(') countSkob--;
            if (isOper(ch[i]) && !checkOp && (ch[i] != '(' && ch[i] != ')')) return false;
            if (isOper(ch[i]) && (ch[i] != '(' && ch[i] != ')')) checkOp = false;
            if (!isOper(ch[i]) || (ch[i] == '(' || ch[i] == ')')) checkOp = true;
            if (ch[i] == '.' && !checkDot) return false;
            if (ch[i] == '.') checkDot = false;
            if (ch[i] != '.') checkDot = true;
        }
        return countSkob == 0;
    }

    private boolean isOper(char ch) {
        return "+-/*()".indexOf(ch) != -1;
    }

    private boolean isOperand(char ch) {
        return "1234567890.".indexOf(ch) != -1;
    }

    private Double calculateStatement(String expr) {
        Deque<Double> valueStack = new ArrayDeque<>();
        Deque<Character> operatorStack = new ArrayDeque<>();

        StringBuilder value;
        for (int i = 0; i < expr.length(); i++) {
            //add number to stack
            if (isOperand(expr.charAt(i))) {
                value = new StringBuilder();
                while(i < expr.length() && isOperand(expr.charAt(i))) {
                    value.append(expr.charAt(i++));
                }
                valueStack.push(Double.parseDouble(value.toString()));
                i--;
            }

            else if (expr.charAt(i) == '('){
                operatorStack.push(expr.charAt(i));
            }

            else if (expr.charAt(i) == ')') {
                while (operatorStack.peek() != '(') {
                    valueStack.push(calculateTwoValues(operatorStack.pop(), valueStack.pop(), valueStack.pop()));
                }
                //to remove '('
                operatorStack.pop();
            }

            else if(expr.charAt(i) == '+' || expr.charAt(i) == '-' || expr.charAt(i) == '*' || expr.charAt(i) == '/'){
                while (!operatorStack.isEmpty() && checkPrecedence(expr.charAt(i), operatorStack.peek())){
                    valueStack.push(calculateTwoValues(operatorStack.pop(), valueStack.pop(), valueStack.pop()));
                }
                operatorStack.push(expr.charAt(i));
            }
        }

        while(!operatorStack.isEmpty()){
            valueStack.push(calculateTwoValues(operatorStack.pop(), valueStack.pop(), valueStack.pop()));
        }
        return valueStack.pop();
    }

    private boolean checkPrecedence(char operand1, char operand2) {
        if (operand2 == '(' || operand2 == ')') {
            return false;
        }
        return !((operand1 == '*' || operand1 == '/') && (operand2 == '+' || operand2 == '-'));
    }

    private Double calculateTwoValues(char operator, double val2, double val1) {
        switch (operator) {
            case '/':
                if (val2 == 0)
                    throw new
                            UnsupportedOperationException("Cannot divide by zero");
                return val1/val2;
            case '*':
                return val1*val2;
            case '+':
                return val1+val2;
            case '-':
                return val1-val2;
            default:
                throw new IllegalArgumentException();
        }
    }
}
