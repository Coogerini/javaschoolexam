package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) ) {
            throw new CannotBuildPyramidException();
        }

        //calculate height and width of pyramid
        int listSize = inputNumbers.size();
        int height = 0;
        while (listSize > 0) {
            if ((listSize -= height + 1) < 0) {
                throw new CannotBuildPyramidException();
            }
            height++;
        }
        int width = 2 * height - 1;

        Collections.sort(inputNumbers);
        int[][] resultPyramid = new int[height][width];

        int currentNumberIndex = inputNumbers.size()-1;
        for (int i = height; i > 0; i--) {
            int zeroGap = height - i;
            for (int j = width - zeroGap; j > zeroGap & currentNumberIndex >= 0; j= j -2) {
                resultPyramid[i-1][j-1] = inputNumbers.get(currentNumberIndex);
                currentNumberIndex--;
            }
        }
        return resultPyramid;
    }


}
